﻿using System.IO;
using System.Threading.Tasks;

namespace Base64EncoderLibrary
{
    public class FileEncoder
    {
        public Task EncodeAsync(string path)
        {
            return EncodeAsync(new FileInfo(path));
        }

        public async Task EncodeAsync(FileInfo file)
        {
            var encoder = new StreamingEncoder();
            using (var writer = new StreamWriter(file.FullName + ".base64.txt"))
            {
                await writer.WriteLineAsync(file.Name);
                await foreach (var line in encoder.EncodeAsync(file.FullName))
                {
                    await writer.WriteLineAsync(line);
                }
            }
        }
    }
}
