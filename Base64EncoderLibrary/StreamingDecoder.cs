﻿using System;
using System.IO;
using System.Threading.Tasks;

namespace Base64EncoderLibrary
{
    public class StreamingDecoder : IDisposable
    {
        public string Path { get; private set; }
        StreamReader Reader { get; set; }
        public bool EndOfStream
        {
            get
            {
                return Reader?.EndOfStream ?? true;
            }
        }

        public StreamingDecoder(string path)
        {
            Path = path;
            Reader = new StreamReader(path);
        }

        public string ReadLine()
        {
            return Reader.ReadLine() ?? throw new ArgumentNullException();
        }

        public byte[] ReadLineAsBytes()
        {
            return Convert.FromBase64String(Reader.ReadLine() ?? throw new ArgumentNullException());
        }

        public async Task<string> ReadLineAsync()
        {
            return await Reader.ReadLineAsync() ?? throw new ArgumentNullException();
        }

        public async Task<byte[]> ReadLineAsBytesAsync()
        {
            return Convert.FromBase64String(await Reader.ReadLineAsync() ?? throw new ArgumentNullException());
        }

        public void Dispose()
        {
            Reader.Dispose();
        }
    }
}
