using Base64EncoderLibrary;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GUI
{
    public partial class Main : Form
    {
        public Main()
        {
            InitializeComponent();
        }

        private async void StartBtn_Click(object sender, EventArgs e)
        {
            ToggleControlState(false);
            if (OperationCmb.SelectedItem.ToString()!.Contains("Encode", StringComparison.InvariantCultureIgnoreCase))
            {
                await EncodeAsync();
            }
            else if (OperationCmb.SelectedItem.ToString()!.Contains("Decode", StringComparison.InvariantCultureIgnoreCase))
            {
                await DecodeAsync();
            }
            ToggleControlState(true);
            MessageBox.Show("Done");
        }

        private void ChooseFileBtn_Click(object sender, EventArgs e)
        {
            using (var ofd = new OpenFileDialog())
            {
                ofd.Filter = "All files (*.*)|*.*";
                ofd.Multiselect = false;
                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    InputTxt.Text = ofd.FileName;
                }
            }
        }

        private async Task EncodeAsync()
        {
            var encoder = new FileEncoder();
            await encoder.EncodeAsync(InputTxt.Text);
        }

        private async Task DecodeAsync()
        {
            var decoder = new FileDecoder();
            await decoder.DecodeAsync(InputTxt.Text);
        }

        private void ToggleControlState(bool enabled)
        {
            foreach(var control in GetControls(this))
            {
                if(!control.Name.Equals("StatusLbl", StringComparison.InvariantCultureIgnoreCase))
                {
                    control.Enabled = enabled;
                }
            }
        }

        private IEnumerable<Control> GetControls(Control control)
        {
            foreach(Control child in control.Controls)
            {
                yield return child;
                foreach(var grandchild in GetControls(child))
                {
                    yield return grandchild;
                }
            }
        }

        private void Main_Load(object sender, EventArgs e)
        {
            OperationCmb.SelectedIndex = 0;
        }
    }
}